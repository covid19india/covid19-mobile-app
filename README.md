# Mobile web app to show [covid19india](https://www.covid19india.org/) data district wise


Built with [`marko-starter`](https://github.com/marko-js/marko-starter)
and [`framework7`](https://framework7.io).

## Install

```bash
npm install
```

## Starting the server

```bash
npm start
```

## Build Static site under /docs

```bash
npm run build:gitlab
```


