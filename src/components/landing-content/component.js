module.exports = class {

    onMount() {
        let dots = '.';
        setInterval(() => {
            this.getEl('loading').innerHTML = `Loading${dots}`
            dots += '.';
        }, 10);

        window.location = 'https://covid19india.gitlab.io/'
    }
}
