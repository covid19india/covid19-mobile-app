module.exports = class {

    onCreate() {
        this.state = {
            selected_total: 0,
            all_district_data: {},
            all: {},
            selected_state: null,
            selected_district: null,
            states: [{ name: "Delhi" }, { name: "Chandigarh" }],
            districts: [{ name: "Rajouri Garden" }, { name: "Ramesh Nagar" }]
        }
    }
    async refresh() {
        // fetch(`${window.app.data.baseUrl}/`)
        // this.state.states = [{ name: "Delhi" }];

        this.getComponent('data-view').setState({
            hidden: true
        })
        this.refresh_source_data().then(() => {
            this.trigger_state_update(this.state.selected_state);
            this.trigger_district_update(this.state.selected_district);
        }).catch((err) => {
            alert('Unable to refresh, try again later');
        })



    }

    async refresh_source_data() {

        let url = 'https://api-stage.lotsoflighting.com/www/covid19india/raw_data.csv';
        // url = `${this.baseUrl()}/static/csv-files/all/all.csv?t=${Date.now()}`;

        return fetch(url).then((response) => response.text()).then((data) => {
            csv({
                output: "csv"
            }).fromString(data)
                .then((results) => {
                    let data = {};
                    for (let row of results) {
                        let exists = data[row[8]] || [];
                        exists.push(row);
                        data[row[8]] = exists;
                    }
                    this.state.all = data;
                    this.state.states = Object.keys(data).map(n => { return { name: n } });
                })

        });
    }

    baseUrl() {
        return document.location.hostname === "localhost" ? "" : "/covid19-mobile-app"
    }
    async onMount() {

        this.refresh_source_data();



    }
    state_changed() {
        this.getComponent('data-view').setState({
            hidden: true
        })
        this.state.selected_state = this.getEl('state').value;
        this.trigger_state_update(this.state.selected_state);
    }
    trigger_state_update(state_name) {

        let state_object = this.state.all[state_name];
        let districts = state_object.reduce((acc, f) => {
            let name = `${f[6] || 'unknown-city'}-${f[7] || 'unkonwn-district'}`;
            let exists = acc[name] || [];
            exists.push(f);
            acc[name] = exists;
            return acc;
        }, {})
        let total = 0;
        for (let key in districts) {
            total = total + districts[key].length;
        }
        this.state.selected_total = total;
        this.state.all_district_data = districts;
        this.state.districts = Object.keys(districts).map((i) => ({ name: i }));
    }

    district_changed() {
        this.state.selected_district = this.getEl('district').value;
        this.trigger_district_update(this.state.selected_district);
    }
    trigger_district_update(district_name) {
        let records = this.state.all_district_data[district_name];
        this.getComponent('data-view').update_records(records);
        this.getComponent('data-view').setState({
            hidden: false,
            selected_total: this.state.selected_total
        })
    }
}
